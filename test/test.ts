import { tap, expect } from '@pushrocks/tapbundle';

import * as npmtsTest from '../ts/index.js';

tap.test('should run through', async () => {
  let testFunction = () => {
    console.log('yes! it works!');
  };
  testFunction();
});

tap.test('should do something', async () => {});

tap.test('should eventually be fullfilled', async () => {});

await tap.start();
