import { tap, expect } from '@pushrocks/tapbundle';

tap.preTask('hi there', async () => {
  console.log('this is a pretask');
});

tap.test('my first test -> expect true to be true', async () => {
  return expect(true).toBeTrue();
});

tap.test('my second test', async (tools) => {
  await tools.delayFor(50);
});

tap.test(
  'my third test -> test2 should take longer than test1 and endure at least 1000ms',
  async () => {}
);

const test4 = tap.skip.test('my 4th test -> should fail', async (tools) => {
  tools.allowFailure();
  expect(false).toBeTrue();
});

const test5 = tap.test('my 5th test -> should pass in about 500ms', async (tools) => {
  tools.timeout(1000);
  await tools.delayFor(500);
});

const test6 = tap.skip.test('my 6th test -> should fail after 1000ms', async (tools) => {
  tools.allowFailure();
  tools.timeout(1000);
  await tools.delayFor(100);
});

await tap.start();
