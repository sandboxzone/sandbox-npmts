/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: 'sandbox-npmts',
  version: '1.0.11',
  description: 'a cool test repo for npmts'
}
